package com.jackie.mediapipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.util.Log;

import com.google.mediapipe.components.FrameProcessor;
import com.google.mediapipe.formats.proto.LandmarkProto;
import com.google.mediapipe.formats.proto.RectProto;
import com.google.mediapipe.framework.AndroidAssetUtil;
import com.google.mediapipe.framework.PacketGetter;
import com.google.mediapipe.glutil.EglManager;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.ByteArrayInputStream;


public class HandTracking {

    private static final String TAG = "Unity HandTracking";

    private static final String BINARY_GRAPH_NAME = "handtrackinggpu.binarypb";
    private static final String INPUT_VIDEO_STREAM_NAME = "input_video";
    private static final String OUTPUT_VIDEO_STREAM_NAME = "output_video";
    private static final String OUTPUT_HAND_PRESENCE_STREAM_NAME = "hand_presence";
    private static final String OUTPUT_LANDMARKS_STREAM_NAME = "hand_landmarks";
    private static final String OUTPUT_PALM_RECT_STREAM_NAME = "hand_rect_from_palm_detections";
    private static final boolean FLIP_FRAMES_VERTICALLY = true;

    static {
        System.loadLibrary("mediapipe_jni");
        System.loadLibrary("opencv_java3");
    }

    private int defaultWithSize = 512;
    private Context context;
    private EglManager eglManager;
    private FrameProcessor processor;
    private MpExternalTextureConverter converter;
    private BmpProducer bitmapProducer;

    private LandmarkProto.NormalizedLandmarkList singleHandLandmarks;
    private RectProto.NormalizedRect palmRect;

    public HandTracking(Context context) {
        this.context = context;
        AndroidAssetUtil.initializeNativeAssetManager(context);

        eglManager = new EglManager(null);

        processor = new FrameProcessor(context, eglManager.getNativeContext(), BINARY_GRAPH_NAME, INPUT_VIDEO_STREAM_NAME, OUTPUT_VIDEO_STREAM_NAME);
        processor.getVideoSurfaceOutput().setFlipY(FLIP_FRAMES_VERTICALLY);

        processor.addPacketCallback(OUTPUT_PALM_RECT_STREAM_NAME, (packet -> {
            byte[] palmRectRaw = PacketGetter.getProtoBytes(packet);
            try {
                RectProto.NormalizedRect palmRect = RectProto.NormalizedRect.parseFrom(palmRectRaw);
                if (palmRect == null) {
                    Log.d(TAG, "[TS:" + packet.getTimestamp() + "] No hand rect.");
                    return;
                }
                this.palmRect = palmRect;
                Log.d(TAG, "[TS:" + packet.getTimestamp() + "] Rect: " + getRectDebugString(palmRect));
            } catch (InvalidProtocolBufferException e) {
                Log.e(TAG, "Couldn't Exception received - " + e);
                return;
            }
        }));

        processor.addPacketCallback(OUTPUT_LANDMARKS_STREAM_NAME, (packet) -> {
            byte[] landmarksRaw = PacketGetter.getProtoBytes(packet);
            try {
                LandmarkProto.NormalizedLandmarkList landmarks = LandmarkProto.NormalizedLandmarkList.parseFrom(landmarksRaw);
                if (landmarks == null) {
                    Log.d(TAG, "[TS:" + packet.getTimestamp() + "] No hand landmarks.");
                    return;
                }
                singleHandLandmarks = landmarks;
                Log.d(TAG, "[TS:" + packet.getTimestamp() + "] #Landmarks for hand: " + landmarks.getLandmarkCount());
                Log.d(TAG, getLandmarksDebugString(landmarks));
            } catch (InvalidProtocolBufferException e) {
                Log.e(TAG, "Couldn't Exception received - " + e);
                return;
            }
        });


        converter = new MpExternalTextureConverter(eglManager.getContext());
        converter.setFlipY(FLIP_FRAMES_VERTICALLY);
        converter.setConsumer(processor);

        bitmapProducer = new BmpProducer(context);
        bitmapProducer.setCustomFrameAvailableListner(converter);
    }

    public void setResolution(int resolution) {
        Log.d(TAG, "set resolution ...");
        this.defaultWithSize = resolution;
        Log.d(TAG, "set resolution success >>>>");
    }

    public float[] getLandmark(int index) {
        Log.d(TAG, "get landmark with index: " + index + "...");
        try {
            LandmarkProto.NormalizedLandmark landmark = this.singleHandLandmarks.getLandmarkList().get(index);
            float[] ret = new float[3];
            ret[0] = landmark.getX();
            ret[1] = landmark.getY();
            ret[2] = landmark.getZ();
            return ret;
        } catch (Exception e) {
            Log.d(TAG, "get landmark with index " + index + " fail <<<<");
            return null;
        }
    }

    public float[] getLandmarks() {
        Log.d(TAG, "get landmarks...");
        if (null == this.singleHandLandmarks) {
            Log.d(TAG, "get landmarks fail <<<<");
            return null;
        }
        float[] landmarks = new float[63];
        for (int i = 0; i < 21; i++) {
            LandmarkProto.NormalizedLandmark landmark = this.singleHandLandmarks.getLandmarkList().get(i);
            landmarks[3 * i] = landmark.getX();
            landmarks[3 * i + 1] = landmark.getY();
            landmarks[3 * i + 2] = landmark.getZ();
        }
        this.singleHandLandmarks = null;
        Log.d(TAG, "get landmarks success >>>>");
        return landmarks;
    }

    public float[] getPalmRect() {
        Log.d(TAG, "get palm rect...");
        if (null == this.palmRect) {
            Log.d(TAG, "get palm rect fail <<<<");
            return null;
        }
        float[] palmRect = new float[5];
        palmRect[0] = this.palmRect.getWidth();
        palmRect[1] = this.palmRect.getHeight();
        palmRect[2] = this.palmRect.getXCenter();
        palmRect[3] = this.palmRect.getYCenter();
        palmRect[4] = this.palmRect.getRotation();
        this.palmRect = null;
        Log.d(TAG, "get palm rect success >>>>");
        return palmRect;
    }

    public void setFrame(byte[] frameSource) {
        Log.d(TAG, "set frame...");
        //attack frame bit map to GPU
        ByteArrayInputStream ims = new ByteArrayInputStream(frameSource);
        Drawable d = Drawable.createFromStream(ims, null);
        Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
        float ratio = ((float) bitmap.getHeight()) / ((float) bitmap.getWidth());
        bitmap = Bitmap.createScaledBitmap(bitmap, defaultWithSize, (int) (defaultWithSize * ratio), true);
        bitmapProducer.loadBmp(bitmap);
        Log.d(TAG, "set frame success >>>>");

    }

    public void test() {
        Log.d(TAG, "test ... ");
        //attack default frame bit map to GPU
        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.img2);
        bitmap = Bitmap.createScaledBitmap(bitmap, 480, 640, true);
        bitmapProducer.loadBmp(bitmap);
        Log.d(TAG, "test success >>>> ");
    }

    public void dispose() {
        converter. close();
    }

    private static String getLandmarksDebugString(LandmarkProto.NormalizedLandmarkList landmarks) {
        int landmarkIndex = 0;
        String landmarksString = "";
        for (LandmarkProto.NormalizedLandmark landmark : landmarks.getLandmarkList()) {
            landmarksString += "\t\tLandmark[" + landmarkIndex + "]: (" + landmark.getX() + ", " + landmark.getY() + ", " + landmark.getZ() + ")\n";
            ++landmarkIndex;
        }
        return landmarksString;
    }

    private static String getRectDebugString(RectProto.NormalizedRect rect) {
        String rectString = "x_center: " + rect.getXCenter() + ", y_center: " + rect.getYCenter() + ", height: " + rect.getHeight() + ", width: " + rect.getWidth();
        return rectString;
    }
}
