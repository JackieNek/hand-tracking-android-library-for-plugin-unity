package com.jackie.mediapipe;

import android.graphics.Bitmap;


public interface CustomFrameAvailableListner {

    public void onFrame(Bitmap bitmap);
}
